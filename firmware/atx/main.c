#include <util/delay.h>
#include <avr/io.h>
#include "io.h"
/*   TTINY45
RST  -------
PB5 -|1 * 8|- VCC
PB3 -|2   7|- PB2
PB4 -|3   6|- PB1
GND -|4   5|- PB0
     -------
*/

void init_adc(){
  ADMUX &=~(1<<REFS0)|(1<<REFS1); // For Aref=Vcc;
  ADCSRA=(1<<ADEN)|(1<<ADPS1)|(1<<ADPS0); //Rrescalar div factor=8
}

uint16_t read_adc(uint8_t ch){
   //Select ADC Channel ch must be 0-7
   ch=ch&0b00000111;
   ADMUX|=ch;

   //Start Single conversion
   ADCSRA|=(1<<ADSC);

   //Wait for conversion to complete
   while(!(ADCSRA & (1<<ADIF)));

   //Clear ADIF by writing one to it
   //Note you may be wondering why we have write one to clear it
   //This is standard way of clearing bits in io as said in datasheets.
   //The code writes '1' but it result in setting bit to '0' !!!

   ADCSRA|=(1<<ADIF);

   return(ADC);
}


int main(){
  uint8_t power_on = 0;
  uint8_t relay_on = 0; // 0 both off, 1, first on, 2 second on.

  BITS_SET_EX(DDRB, PB0, PB1, PB2);

  BITS_SET(PORTB, PB0, PB1, PB2);
  BITS_SET(PORTB, PB3, PB4);

  init_adc();

  while(1){
    _delay_ms(20);

    int button = (read_adc(3)*5+512)/1024;

    switch(button){
      case 0:
        power_on ^= 1;
        if (power_on == 1){
          BITS_CLEAR(PORTB, PB0);
          _delay_ms(300);
        } else {
          BITS_SET(PORTB, PB0);
          relay_on = 0;
          _delay_ms(300);
        }
      break;
        case 2:
        relay_on--;
        _delay_ms(300);
        break;
      case 3:
        //BITS_TOOGLE(PORTB, PB1);
        relay_on++;
        _delay_ms(300);
      break;
    }
    
    if (!BIT_GET(PINB, PB4) && power_on == 1){
      BITS_SET(PORTB, PB0);
      power_on = 0;
      relay_on = 0;
      _delay_ms(5000);
    }
  
    if (relay_on > 2) relay_on = 0;
    if (relay_on == 1){
      BITS_CLEAR(PORTB, PB2);
      BITS_SET(PORTB, PB1);
    } else if (relay_on == 2){
      BITS_CLEAR(PORTB, PB1);
      BITS_SET(PORTB, PB2);
    } else {
      BITS_SET(PORTB, PB1, PB2);
    }

  }
  return 0;
}
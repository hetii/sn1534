#include <util/delay.h>
#include <avr/interrupt.h>
#include <avr/io.h>
#include <stdio.h>
#include "io.h"
#include "ds18b20.h"

/*   TTINY45
RST  -------
PB5 -|1 * 8|- VCC
PB3 -|2   7|- PB2
PB4 -|3   6|- PB1
GND -|4   5|- PB0
     -------
*/

#define BAUD_RATE 9600

#define UART_TX PINB1
/* account for integer truncation by adding 3/2 = 1.5 */
# define TXDELAY (int)(((F_CPU/BAUD_RATE)-7 +1.5)/3)

// Initialise the UART
void uart_init() {
    // Set up TX pin
    DDRB |= (1 << UART_TX);
    PORTB |= (1 << UART_TX);
}

/** Write a single character
 * Send a single character on the UART.
 * @param ch the character to send.
 */
void uart_send(char ch) {
    // Set to output state and bring high
    PORTB |= (1 << UART_TX);
    cli();
    asm volatile(
        "  cbi %[uart_port], %[uart_pin]    \n\t"  // start bit
        "  in r0, %[uart_port]              \n\t"
        "  ldi r30, 3                       \n\t"  // stop bit + idle state
        "  ldi r28, %[txdelay]              \n\t"
        "TxLoop:                            \n\t"
        // 8 cycle loop + delay - total = 7 + 3*r22
        "  mov r29, r28                     \n\t"
        "TxDelay:                           \n\t"
        // delay (3 cycle * delayCount) - 1
        "  dec r29                          \n\t"
        "  brne TxDelay                     \n\t"
        "  bst %[ch], 0                     \n\t"
        "  bld r0, %[uart_pin]              \n\t"
        "  lsr r30                          \n\t"
        "  ror %[ch]                        \n\t"
        "  out %[uart_port], r0             \n\t"
        "  brne TxLoop                      \n\t"
        :
        : [uart_port] "I" (_SFR_IO_ADDR(PORTB)),
          [uart_pin] "I" (UART_TX),
          [txdelay] "I" (TXDELAY),
          [ch] "r" (ch)
        : "r0","r28","r29","r30");
    sei();
}

void init_adc(){
  ADMUX &=~(1<<REFS0)|(1<<REFS1); // For Aref=Vcc;
  ADCSRA=(1<<ADEN)|(1<<ADPS1)|(1<<ADPS0); //Rrescalar div factor=8
}


uint16_t read_adc(uint8_t ch){
   //Select ADC Channel ch must be 0-7
   ch=ch&0b00000111;
   ADMUX|=ch;

   //Start Single conversion
   ADCSRA|=(1<<ADSC);

   //Wait for conversion to complete
   while(!(ADCSRA & (1<<ADIF)));

   //Clear ADIF by writing one to it
   //Note you may be wondering why we have write one to clear it
   //This is standard way of clearing bits in io as said in datasheets.
   //The code writes '1' but it result in setting bit to '0' !!!

   ADCSRA|=(1<<ADIF);

   return ADC;
}

void string_usart(char * text){
    while(*text){
        uart_send(*text++);
        _delay_ms(10);
    }
}

void pwm_init(){

    // Set Timer 0 prescaler to clock/8.
    TCCR0B |= (1 << CS01);
    //TCCR0B |= (1 << CS01)|(1 << CS00);
    
    // Set to 'Phase correct PWM' mode
    TCCR0A |= (1 << WGM02) | (1 << WGM00);
    //TCCR0A |= (1 << WGM01) | (1 << WGM00);
 
    // Clear OC0B output on compare match, upwards counting.
    TCCR0A |= (1 << COM0A1);
}

void pwm_write (int val){
    OCR0A = val;
}

int main(){
  /* 
    PB0 == FAN
    PB1 == Relay
    PB3 == RX of RS232
    PB4 == PSU OFF when +5v.
  */

  char buf[50];

  BITS_SET_EX(DDRB, PB0, PB1, PB4); // PB3 instead reset for test.
  BITS_CLEAR(PORTB, PB0, PB1, PB4);

  init_adc();
  //uart_init();
  //pwm_init();
  //pwm_write(60);
  //pwm_write(70);  // 8.8v
  //pwm_write(100); // 9.5v
  //pwm_write(130); // 10.5v
  //pwm_write(160); // 11v
  //pwm_write(190); // 11.5v

  while(1){

    _delay_ms(500);

    uint16_t v = read_adc(1);

    // 363 == ~19.5v
    if (v > 363) {
      BITS_SET(PORTB, PB1); // PB1 instead reset:P
    } else {
      BITS_CLEAR(PORTB, PB1);
    }

    int t = ds18b20_gettemp();
    /*
        sprintf(buf, "ADC: %" PRIu16 ", Temp: %d\r\n", v, t);
        string_usart(buf);
    */
    // PSU on
    if ( t <= 65){
      BITS_CLEAR(PORTB, PB4);
    }

    // Disable FAN
    if ( t < 45){
      BITS_CLEAR(PORTB, PB0);
    }

    // PSU off
    if (t >= 75){
      BITS_SET(PORTB, PB4);
    /*     
    } else if (t >= 70){

    } else if (t >= 65){

    } else if (t >= 60){

    } else if (t >= 55){
    */
    } else if (t >= 50){
      BITS_SET(PORTB, PB0);
    }
  }
  return 0;
}
